#-------------------------------------------------
#
# Project created by QtCreator 2017-04-20T16:27:31
#
#-------------------------------------------------

QT          += core gui widgets opengl
LIBS        +=  -lopengl32
CONFIG += C++11

QMAKE_LFLAGS_RELEASE += -static -static-libgcc

INCLUDEPATH += include

#greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET      = task_dssl
TEMPLATE    = app

DEFINES     += QT_DEPRECATED_WARNINGS

SOURCES     += src\main.cpp\
                src\mainwindow.cpp \
                src\glwindow.cpp \
                src\ball.cpp

HEADERS     += include\mainwindow.h \
            include\glwindow.h \
            include\ball.h

FORMS       += ui\mainwindow.ui

#ifndef GLWINDOW_H
#define GLWINDOW_H

#include <QtOpenGL/QGLWidget>
#include <QMouseEvent>
#include <ball.h>

#include <vector>
#include <QTimer>

extern std::vector<Ball *> balls;

class GLWindow : public QGLWidget//, protected QGLFunctions
{
Q_OBJECT
private:
    QGLFormat gl_format;
    int width, height;

    void loadSettingsGL();
    void loadSettingsGL(int _width, int _height);

    int searchBall(GLfloat _x, GLfloat _y);
    bool checkEnterBall(GLfloat _x, GLfloat _y);

    GLint mouse_x, mouse_y;
    GLfloat radius_ball_default;

    int index_ball_grab;

    bool release_button_for_translate;
    bool release_translate;
    bool generate_balls_once;

protected:
    virtual void initializeGL();
    virtual void resizeGL(int nWidth, int nHeight);
public slots:
    virtual void paintGL();

    virtual void mouseMoveEvent(QMouseEvent *event);
    virtual void mousePressEvent(QMouseEvent *event);
    virtual void mouseReleaseEvent(QMouseEvent *event);

public:
    explicit GLWindow(QWidget *parent = 0);
    ~GLWindow();

    GLVector2f randomDot()
    {
        srand(time(NULL));
        GLVector2f temp((GLfloat)((rand() % this->width) - this->radius_ball_default), (GLfloat)((rand() % this->height) - this->radius_ball_default));
        temp.check_dot(this->width, this->height, this->radius_ball_default);
        bool temp_correct = false;
        while(!temp_correct)
        {
            temp_correct = this->checkEnterBall(temp.x, temp.y);
            if(!temp_correct)
            {
                temp.setVector(rand() % this->width, rand() % this->height);
                temp.check_dot(this->width, this->height, this->radius_ball_default);
            }
        }
        return temp;
    }

    QTimer *animation;

signals:

public slots:
};

#endif // GLWINDOW_H

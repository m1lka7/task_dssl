#ifndef BALL_H
#define BALL_H

#include <GL/gl.h>
#include <cmath>

struct GLVector2f {
    GLfloat x, y;

    GLVector2f() { }
    GLVector2f(GLfloat _x, GLfloat _y)
    {
        this->x = _x;
        this->y = _y;
    }

    void setVector(GLfloat _x, GLfloat _y)
    {
        this->x = _x;
        this->y = _y;
    }

    void check_dot(GLfloat _x, GLfloat _y, GLfloat _radius)
    {
        if(this->x < _radius*2.0f)
            this->x = _radius*2.0f;
        if(this->x >= _x - _radius)
            this->x = _x - _radius*2.0f;
        if(this->y < _radius*2.0f)
            this->y = _radius*2.0f;
        if(this->y >= _y - _radius)
            this->y = _y - _radius*2.0f;
    }
};


class Ball
{
public:
    Ball() { }
    Ball(GLfloat _x, GLfloat _y, GLfloat _radius);
    Ball(GLVector2f _position, GLfloat _radius);

    ~Ball() { }

    GLfloat radius;
    GLVector2f position;
    GLfloat fx, fy;
    bool activate;

    void paint();
    void translate(GLfloat _x, GLfloat _y);

    void forceEnabled(Ball *otherBall);
    void move();

    bool destroy();
};


#endif // BALL_H

#include "glwindow.h"
#include <GL/gl.h>
#include <math.h>

std::vector<Ball *> balls;

GLWindow::GLWindow(QWidget *parent) : QGLWidget(parent)
{
    this->gl_format.setDirectRendering(true);
    this->gl_format.setDoubleBuffer(true);
    this->gl_format.setSampleBuffers(true);
    this->gl_format.setSwapInterval(1);

    setFormat(this->gl_format);
    setFocusPolicy(Qt::StrongFocus);

    this->width = 0;
    this->height = 0;

    this->mouse_x = 0;
    this->mouse_y = 0;

    this->radius_ball_default = 25.0f;
    this->release_button_for_translate = false;
    this->release_translate = false;

    this->index_ball_grab = -1;

    setMouseTracking(true);

    this->generate_balls_once = false;
    animation = new QTimer(this);
    connect(animation, SIGNAL(timeout()), this, SLOT(paintGL()));
    connect(animation, SIGNAL(timeout()), this, SLOT(updateGL()));
    animation->start(1000/60);
}

GLWindow::~GLWindow()
{
    glDisable(GL_POLYGON_SMOOTH);
}

void GLWindow::loadSettingsGL()
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glViewport(0, 0, (GLint)this->width, (GLint)this->height);
    glOrtho(0, this->width, this->height, 0, -1, 1);
}

void GLWindow::loadSettingsGL(int _width, int _height)
{
    this->width = _width;
    this->height = _height;

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glViewport(0, 0, (GLint)this->width, (GLint)this->height);
    glOrtho(0, this->width, this->height, 0, -1, 1);
}

int GLWindow::searchBall(GLfloat _x, GLfloat _y)
{
    for(unsigned int i = 0; i < balls.size(); i++)
    {
        if( (((balls[i]->position.x - _x) * (balls[i]->position.x - _x) +
             (balls[i]->position.y - _y) * (balls[i]->position.y - _y)) <
                balls[i]->radius * balls[i]->radius))
        {
            return (int)i;
        }
    }
    return -1;
}

bool GLWindow::checkEnterBall(GLfloat _x, GLfloat _y)
{
    if(!balls.empty())
    {
        for(auto ball: balls) {
            GLfloat x = ball->position.x - _x;
            GLfloat y = ball->position.y - _y;
            if(sqrtf((x*x) + (y*y)) < (this->radius_ball_default * 2.0f))
            {
                return false;
            }
        }
    }
    return true;
}

void GLWindow::initializeGL()
{
    qglClearColor(Qt::black);

    this->loadSettingsGL();

    glEnable(GL_DEPTH_TEST);
    glClearDepth(1.0f);
    glDepthFunc(GL_LESS);
    glEnable(GL_DEPTH_CLAMP);
    glEnable(GL_POLYGON_SMOOTH);
    glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);

    qglColor(Qt::red);
}

void GLWindow::resizeGL(int nWidth, int nHeight)
{
    this->loadSettingsGL(nWidth, nHeight);
}

void GLWindow::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    if(!this->generate_balls_once) {
        for(int i = 0; i < 10; i++) {
            balls.push_back(new Ball(this->randomDot(), this->radius_ball_default));
        }
        this->generate_balls_once = true;
    }

    for(auto ball: balls)
    {
        ball->paint();
    }
}

void GLWindow::mouseMoveEvent(QMouseEvent *event)
{
    this->mouse_x = event->x();
    this->mouse_y = event->y();
    if(this->release_button_for_translate)
    {
        if(this->index_ball_grab >= 0) {
            balls[this->index_ball_grab]->translate(this->mouse_x, this->mouse_y);
            this->release_translate = true;
            if(this->checkEnterBall(this->mouse_x, this->mouse_y))
            {
                balls[this->index_ball_grab]->translate(balls[this->index_ball_grab]->position.x - 50.0f,
                        balls[this->index_ball_grab]->position.y - 50.0f);

            }
        }
    }
    updateGL();
}

void GLWindow::mousePressEvent(QMouseEvent *event)
{
    switch(event->button()) {
        case Qt::LeftButton:
            this->index_ball_grab = this->searchBall(event->x(), event->y());
            this->release_button_for_translate = true;
            break;
        default: break;
    };

    updateGL();
}

void GLWindow::mouseReleaseEvent(QMouseEvent *event)
{
    switch (event->button()) {
    case Qt::LeftButton:
        this->release_button_for_translate = false;
        if(!balls.empty() && !this->release_translate)
        {
            int index_ball_delete = this->searchBall(event->x(), event->y());
            if(index_ball_delete >= 0)
            {
                delete balls[index_ball_delete];
                balls.erase(balls.begin() + index_ball_delete);
            }
        }
        this->release_translate = false;
        this->index_ball_grab = -1;
        break;

    case Qt::RightButton:
        if(this->checkEnterBall(event->x(), event->y()))
        {
            balls.push_back(new Ball(event->x(), event->y(), this->radius_ball_default));
        }
        break;

    default:
        break;
    }
    updateGL();
}



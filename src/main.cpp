#include "mainwindow.h"
#include <QApplication>
#include <thread>
#include <mutex>

std::mutex ballMutex;

void forceActivate()
{
    while(true)
    {
        ballMutex.lock();
        for(unsigned int i = 0; i < balls.size(); i++)
        {
            for(unsigned int j = 0; j < balls.size(); j++)
            {
                balls[i]->forceEnabled(balls[j]);
            }
        }

        for(unsigned int i = 0; i < balls.size(); i++)
        {
            balls[i]->move();
        }
        ballMutex.unlock();
        std::this_thread::sleep_for(std::chrono::milliseconds(1000/60));
        //std::cout << "unsleep: " << clock() << std::endl;
    }
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    std::thread threadCalc(forceActivate);
    threadCalc.detach();
    w.show();

    return a.exec();
}

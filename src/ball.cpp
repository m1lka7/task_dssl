#include "include\ball.h"
#include <QtOpenGL/QGLWidget>

Ball::Ball(GLfloat _x, GLfloat _y, GLfloat _radius)
{
    this->position.x = _x;
    this->position.y = _y;
    this->radius = _radius;
    this->activate = false;
    this->fx = 0.0;
    this->fy = 0.0;
}

Ball::Ball(GLVector2f _position, GLfloat _radius)
{
    this->position = _position;
    this->radius = _radius;
    this->activate = false;
    this->fx = 0.0;
    this->fy = 0.0;
}

void Ball::paint()
{
    glBegin(GL_TRIANGLE_FAN);
        glVertex2f(this->position.x, this->position.y); // вершина в центре круга
        for(int angle = 0; angle <= 360; angle+=10)
        {
            GLfloat x = this->radius * cos(angle * 3.1415f / 180) + 1.0f;
            GLfloat y = this->radius * sin(angle * 3.1415f / 180) + 1.0f;
            glVertex2f(x + this->position.x, y + this->position.y);
        }
    glEnd();
    glFlush();
}

void Ball::translate(GLfloat _x, GLfloat _y)
{
    this->position.x = _x;
    this->position.y = _y;
}

void Ball::forceEnabled(Ball *otherBall)
{
    if(otherBall == this)
        return;

    GLfloat otherBall_x = otherBall->position.x;
    GLfloat otherBall_y = otherBall->position.y;
    GLfloat r = sqrtf(pow(otherBall_x - this->position.x, 2) + pow(otherBall_y - this->position.y, 2));

    if(r < 25)
        return;

    GLfloat force = 1/r - 1/(r*r);
    this->fx += (force*(otherBall_x - this->position.x)/r)*(1000/60);
    this->fy += (force*(otherBall_y - this->position.y)/r)*(1000/60);
}

void Ball::move()
{
    this->position.x += this->fx;
    this->position.y += this->fy;
    this->fx = 0.0;
    this->fy = 0.0;
}

bool Ball::destroy()
{
    this->~Ball();
    return true;
}


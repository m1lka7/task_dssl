#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    main_gl_window = new GLWindow(this);
    ui->OGLGridLayout->addWidget(main_gl_window);
}

MainWindow::~MainWindow()
{
    delete ui;
    delete main_gl_window;
}

void MainWindow::on_closeButton_clicked()
{
    close();
}
